package br.com.pagseguro.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import br.com.pagseguro.model.Stock;

public interface StockService {

	/**
	 * Method to create new stocks in the db using mongo-db repository.
	 * @param emp
	 */
	public void createStock(List<Stock> stocks);

	/**
	 * Method to fetch all stocks from the db using mongo-db repository.
	 * @return
	 */
	public Collection<Stock> getAllStocks();

	/**
	 * Method to fetch stock by id using mongo-db repository.
	 * @param id
	 * @return
	 */
	public Optional<Stock> findStockById(String id);

	/**
	 * Method to delete stock by id using mongo-db repository.
	 * @param id
	 */
	public void deleteStockById(String id);

	/**
	 * Method to update stock by id using mongo-db repository.
	 * @param id
	 */
	public void updateStock(Stock stock);

	/**
	 * Method to delete all stocks using mongo-db repository.
	 */
	public void deleteAllStocks();
	
	/**
	 * Method to findbycode stock using mongo-db repository.
	 */
	public Stock findStockByCode(String code);
	
	/**
	 * Method to findbyname stock using mongo-db repository.
	 */
	public Stock findStockByName(String name);
	
	/**
	 * Method to getTopTen  using  mongo-db repository and apidojo-yahoo-finance-v1.
	 */
	public Collection<Stock> getTopTen();
	
	/**
	 * Method to update alls symbols in stocks.
	 */
	public void updateSymbols();
}