package br.com.pagseguro.apidojo;

public class QuarterlyVO{
    public String date;
    public double actual;
    public double estimate;
    public Object revenue;
    public long earnings;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getActual() {
		return actual;
	}
	public void setActual(double actual) {
		this.actual = actual;
	}
	public double getEstimate() {
		return estimate;
	}
	public void setEstimate(double estimate) {
		this.estimate = estimate;
	}
	public Object getRevenue() {
		return revenue;
	}
	public void setRevenue(Object revenue) {
		this.revenue = revenue;
	}
	public long getEarnings() {
		return earnings;
	}
	public void setEarnings(long earnings) {
		this.earnings = earnings;
	}
}