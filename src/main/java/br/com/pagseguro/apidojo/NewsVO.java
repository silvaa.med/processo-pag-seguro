package br.com.pagseguro.apidojo;

public class NewsVO{
    public String uuid;
    public String title;
    public String publisher;
    public String link;
    public int providerPublishTime;
    public String type;
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public int getProviderPublishTime() {
		return providerPublishTime;
	}
	public void setProviderPublishTime(int providerPublishTime) {
		this.providerPublishTime = providerPublishTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
