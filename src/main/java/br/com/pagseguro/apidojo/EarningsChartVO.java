package br.com.pagseguro.apidojo;

import java.util.List;

public class EarningsChartVO{
    public List<QuarterlyVO> quarterly;
    public String currentQuarterEstimateDate;
    public int currentQuarterEstimateYear;
    public List<Integer> earningsDate;
    
	public List<QuarterlyVO> getQuarterly() {
		return quarterly;
	}
	public void setQuarterly(List<QuarterlyVO> quarterly) {
		this.quarterly = quarterly;
	}
	public String getCurrentQuarterEstimateDate() {
		return currentQuarterEstimateDate;
	}
	public void setCurrentQuarterEstimateDate(String currentQuarterEstimateDate) {
		this.currentQuarterEstimateDate = currentQuarterEstimateDate;
	}
	public int getCurrentQuarterEstimateYear() {
		return currentQuarterEstimateYear;
	}
	public void setCurrentQuarterEstimateYear(int currentQuarterEstimateYear) {
		this.currentQuarterEstimateYear = currentQuarterEstimateYear;
	}
	public List<Integer> getEarningsDate() {
		return earningsDate;
	}
	public void setEarningsDate(List<Integer> earningsDate) {
		this.earningsDate = earningsDate;
	}
}
