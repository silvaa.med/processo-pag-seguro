package br.com.pagseguro.dao;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import br.com.pagseguro.model.Stock;

public class CustomStockDaoImpl implements CustomStockDao {
	
	@Autowired
    private MongoTemplate mongoTemplate;
	
	@Override
    public Collection<Stock> getTopTen() {
    	
    	Query query = new Query();
    	query.limit(10);
    	query.with(Sort.by(Sort.Direction.DESC, "participation"));
    	Collection<Stock> stocks = mongoTemplate.find(query,Stock.class);
    	return stocks;
    }

}
