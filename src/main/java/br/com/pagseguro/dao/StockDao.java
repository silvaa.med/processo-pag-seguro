package br.com.pagseguro.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.pagseguro.model.Stock;

@Repository
public interface StockDao extends MongoRepository<Stock, String>, CustomStockDao{
	
	public Stock findStockByCode(String code);
	
	public Stock findStockByName(String name);
}