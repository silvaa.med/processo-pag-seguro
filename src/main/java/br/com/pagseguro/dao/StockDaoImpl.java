package br.com.pagseguro.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import br.com.pagseguro.model.Stock;

public abstract class StockDaoImpl implements StockDao, CustomStockDao{
	
    @Autowired
    private MongoTemplate mongoTemplate;
    
    @Override
    public Stock findStockByCode(String code) {

        Query query = new Query();
        query.addCriteria(Criteria.where("code").is(code));

        return mongoTemplate.findOne(query, Stock.class);
    }

    @Override
    public Stock findStockByName(String name) {

        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));

        return mongoTemplate.findOne(query, Stock.class);
    }
}
