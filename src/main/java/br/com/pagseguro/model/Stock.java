package br.com.pagseguro.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// Mongo database annotation.
@Document(collection= "stock")
public class Stock {

	@Id
	private String id;
	private String code;
	private String name;
	private long amount;
	private double participation;
	private String symbol;
	private double regularMarketPrice;

	public Stock() {	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public double getParticipation() {
		return participation;
	}

	public void setParticipation(double participation) {
		this.participation = participation;
	}

	public double getRegularMarketPrice() {
		return regularMarketPrice;
	}

	public void setRegularMarketPrice(double regularMarketPrice) {
		this.regularMarketPrice = regularMarketPrice;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@Override
	public String toString() {
		return "Stock [id=" + id + ", code=" + code + ", name=" + name + ", amount=" + amount + ", participation=" + participation + ", regularMarketPrice=" + regularMarketPrice + ", symbol=" + symbol+ "]";
	}
}