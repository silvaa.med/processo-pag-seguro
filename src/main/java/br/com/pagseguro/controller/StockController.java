package br.com.pagseguro.controller;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pagseguro.model.Stock;
import br.com.pagseguro.service.StockService;

@RestController
@RequestMapping(value= "/stocks")
public class StockController {

	@Autowired
	StockService serv;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Method to save stocks in the db.
	 * @param stock
	 * @return
	 */
	@PostMapping()
	public String create(@RequestBody List<Stock> stock) {
		logger.debug("Saving stocks.");
		serv.createStock(stock);
		return "Stock records created.";
	}

	/**
	 * Method to fetch all stocks from the db.
	 * @return
	 */
	@GetMapping()
	public Collection<Stock> getAll() {
		logger.debug("Getting all stocks.");
		return serv.getAllStocks();
	}

	/**
	 * Method to fetch stock by id.
	 * @param id
	 * @return
	 */
	@GetMapping(value= "/{stock-id}")
	public Optional<Stock> getById(@PathVariable(value= "stock-id") String id) {
		logger.debug("Getting stock with stock-id= {}.", id);
		return serv.findStockById(id);
	}

	/**
	 * Method to update stock by id.
	 * @param id
	 * @param e
	 * @return
	 */
	@PutMapping(value= "/{stock-id}")
	public String update(@PathVariable(value= "stock-id") String id, @RequestBody Stock stock) {
		logger.debug("Updating stock with stock-id= {}.", id);
		stock.setId(id);
		serv.updateStock(stock);
		return "Stock record for stock-id= " + id + " updated.";
	}

	/**
	 * Method to delete stock by id.
	 * @param id
	 * @return
	 */
	@DeleteMapping(value= "/{stock-id}")
	public String delete(@PathVariable(value= "stock-id") String id) {
		logger.debug("Deleting stock with stock-id= {}.", id);
		serv.deleteStockById(id);
		return "Stock record for stock-id= " + id + " deleted.";
	}

	/**
	 * Method to delete all stocks from the db.
	 * @return
	 */
	@DeleteMapping(value= "/deleteall")
	public String deleteAll() {
		logger.debug("Deleting all stocks.");
		serv.deleteAllStocks();
		return "All stock records deleted.";
	}
	
	/**
	 * Method to fetch stock by code.
	 * @param code
	 * @return
	 */
	@GetMapping(value= "/code/{stock-code}")
	public Stock getByCode(@PathVariable(value= "stock-code") String code) {
		logger.debug("Getting stock with stock-code= {}.", code);
		return serv.findStockByCode(code);
	}
	

	/**
	 * Method to fetch stock by name.
	 * @param name
	 * @return
	 */
	@GetMapping(value= "/name/{stock-name}")
	public Stock getByName(@PathVariable(value= "stock-name") String name) {
		logger.debug("Getting stock with stock-name= {}.", name);
		return serv.findStockByName(name);
	}
	
	/**
	 * Method to fetch stock by top 10 participation.
	 * @return
	 */
	@GetMapping(value= "/topten")
	public Collection<Stock>  getTopTen() {
		logger.debug("Getting stock with top 10 participation.");
		return serv.getTopTen();
	}
	
	/**
	 * Method to fetch stock by top 10 participation.
	 * @return
	 */
	@GetMapping(value= "/updateallsymbols")
	public void  updateSymbols() {
		logger.debug("Getting stock with top 10 participation.");
	    serv.updateSymbols();
	}
}