# Rodando no Docker

### Instruções


#####Para rodar o banco de dados no docker utilize os comandos abaixo:
* ```docker pull mongo```
* ```docker run -it --name mongodb -d -p 27017:27017 -p 28017:28017 -d -v C:\Temp:/var/tmp mongo```
* ```docker exec -it {container_id} /bin/bash```
* Jogar o arquivo com as acoes no C:\Temp. (Fiz algumas alterações do arquivo original que achei relevante)
* O arquivo stocks.csv esta na pasta resources do projeto.
* ```dpkg -l mongodb-database-tools``` para instalar o mongoimport

##### Para importar as ações para o banco utilizei o comando
* ```mongoimport -d finance -c stock --type csv --file /var/tmp/stocks.csv --headerline``` 

* Para iniciar o banco criar index e fazer as querys.
* ```mongo```
* ```show dbs```
* ```use finance```
* ```show collections```
* ```db.stock.find();```
* ```db.stock.createIndex( { "name": "text" } ) ```
* ```db.stock.find().sort( { participation: -1 } ).limit(10)```


#####Para rodar a aplicação no docker execute os comandos abaixo:

 * ```./mvnw clean package -DskipTests```
* Criar a imagem com o dockerfile```docker build -t pagseguro-app . ``` 
* Executar o ambiente com: ```docker run -p 8102:8102 pagseguro-app```
* Servidor rodando em ```http://localhost:8102/stocks``



#####Também criei um database no MongoDB Atlas
* ```mongodb+srv://mongodb:mongodb@cluster0.jngbq.mongodb.net/finance?retryWrites=true```

#####Em resources segue também o json do postman com os endpoints
* ```https://app.swaggerhub.com/apis-docs/BSCG/pag-seguro/1.0#/```